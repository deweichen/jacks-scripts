#!/usr/bin/python

# Scan a project using the Jacks API
# Author: Dewei Chen
# 
# This script has dependency on the python requests package
# To install with the python package manager pip run: pip install requests

import sys
import json
import time
import pprint
import requests

API_URL = "https://jacks-api.codiscope.com"
USER_EMAIL = "dchen@codiscope.com"
USER_PASS = ':RS4PP0"Sh'
PROJECT_ID = "e620c078-20a0-4a97-a81f-a3a7bc6bea12"

def login(email, password):
  url = API_URL + "/jacks/auth/login"
  data = { 'email': email, 'password': password }
  r = requests.post(url, data=data)
  if r.status_code != 200:
    print "Failed to login"
    sys.exit()
  body = json.loads(r.text)
  return body['data']['token']

def get_projects(token):
  url = API_URL + "/jacks/project/list"
  headers = { 'authorization': 'Bearer ' + token }
  r = requests.get(url, headers=headers)
  if r.status_code != 200:
    print "Failed to list projects"
    sys.exit()
  body = json.loads(r.text)
  return body['data']

def scan_project(token, project_id):
  print "scanning project %r" % project_id
  url = API_URL + "/jacks/scan/project"
  headers = { 'authorization': 'Bearer ' + token }
  data = { 'projectId': project_id }
  r = requests.post(url, data=data, headers=headers)
  if r.status_code != 200:
    print "Failed to scan project"
    sys.exit()
  body = json.loads(r.text)
  scan_id = body['data']['id']

  # poll api for scan status to be finished
  status_url = API_URL + "/jacks/scan/project/status"
  params = { 'scanId': scan_id }
  scan_finished = False
  print "waiting for scan to finish.."
  while not scan_finished:
    time.sleep(2)
    r = requests.get(status_url, params=params,  headers=headers)
    if r.status_code != 200:
      print "Failed to poll for scan status"
      sys.exit()
    body = json.loads(r.text)
    scan_finished = (body['data']['status'] == "completed")
  return scan_id

def get_guidances(token, scan_id):
  url = API_URL + "/jacks/guidance/project/categories"
  headers = { 'authorization': 'Bearer ' + token }
  params = { 'scanId': scan_id }
  r = requests.get(url, params=params, headers=headers)
  if r.status_code != 200:
    print "Failed to list guidances"
    sys.exit()
  body = json.loads(r.text)
  guidances = body['data']['guidances']
  print "== guidances =="
  pprint.pprint(guidances)
  return guidances

def main():
  token = login(USER_EMAIL, USER_PASS)
  # scan the hard-coded PROJECT_ID or the first project returned from list_projects
  project_id = PROJECT_ID or get_projects(token)[0]['id']
  scan_id = scan_project(token, project_id)
  get_guidances(token, scan_id)


if __name__ == '__main__':
  main()