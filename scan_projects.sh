#!/bin/bash

# Scan a project using the Jacks API
# Author: Dewei Chen
# 
# Dependency python-2.6+

# Project Id to scan
projectid=""

# Authentication
useremail=""
userpass=""

# API URLs
loginurl="https://jacks-api.codiscope.com/jacks/auth/login"
projectsurl="https://jacks-api.codiscope.com/jacks/project/list"
scanurl="https://jacks-api.codiscope.com/jacks/scan/project"
statusurl="https://jacks-api.codiscope.com/jacks/scan/project/status"
guidanceurl="https://jacks-api.codiscope.com/jacks/guidance/project/categories"

function login {
  token=$(curl -s -XPOST $loginurl -d email=$useremail -d password=$userpass | \
  python -c "import json, sys; obj=json.load(sys.stdin); print obj['data']['token']")
}

function getprojects {
  curl -s -XGET $projectsurl -H "Authorization: Bearer $token" | \
  python -m json.tool
  # python -c "import json, sys; obj=json.load(sys.stdin); print obj['data']")
}

function scanstatus {
  status=$(curl -s -XGET $statusurl -H "Authorization: Bearer $token" -Gd scanId=$scanid | \
  python -c "import json, sys; obj=json.load(sys.stdin); print obj['data']['status']")
}

function scanproject {
  scanid=$(curl -s -XPOST $scanurl -H "Authorization: Bearer $token" -d projectId=$projectid | \
  python -c "import json, sys; obj=json.load(sys.stdin); print obj['data']['id']")
  echo "Scan id: $scanid"
  status="pending"
  while [ $status != "completed" ]; do
    sleep 2
    scanstatus
    echo "Scan status: $status"
  done
}

function getguidances {
  curl -s -XGET $guidanceurl -H "Authorization: Bearer $token" -Gd scanId=$scanid | \
  python -m json.tool
}


# Command line arguments

function showhelp {
    cat << EOF
Usage: ${0##*/} [-u] [-w] [-p]
Run a scan on a Jacks project

    -u {email}            Required: User email address 
    -w {password}         Required: User password
    -p {projectid}        Optional: Scan a project with id, omit to list all projects

Examples:
(Scan project)  ./scan_projects.sh -u "dchen@codiscope.com" -w "mypass" -p e620c078-20a0-4a97-a81f-a3a7bc6bea12
(List projects) ./scan_projects.sh -u "dchen@codiscope.com" -w "mypass"

EOF
}

while [[ $# > 1 ]] 
do
key="$1"
    case "$key" in
        -u)  
            useremail="$2"
            shift
            ;;
        -w)
            userpass="$2"
            shift
            ;;
        -p)
            projectid="$2"
            shift
            ;;
    esac
shift
done

if [[ ! -z $projectid && ! -z $useremail && ! -z $userpass ]]
then
  echo "Scan project"
  login
  scanproject
  getguidances
elif [[ ! -z $useremail && ! -z $userpass ]]
then
  echo "List projects"
  login
  getprojects
else
  showhelp
  exit 0;
fi